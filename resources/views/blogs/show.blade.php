<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Blog Details</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    @include('layouts.navbar')

    <div class="container my-3">
        <h2 class="text-center m-3">Blog Details <a href="{{route('blog')}}" class="btn btn-secondary mx-2 float-end">Go to blogs</a></h2>
        <div class="row mb-2">
            <div class="col-sm-4">
                <div class="img-post">
                    @if ($blog->thumbnail)
                    <img class="d-block img-fluid" width="400" height="800" src="{{ asset('uploads/' . $blog->thumbnail) }}" alt="{{ $blog->title }}" />
                    @else
                    <img class="d-block img-fluid" src="" alt="not available bolg thumbnail">
                    @endif
                </div>
            </div>
            <div class="col-sm-8">
            <h3 class="mb-0">{{ $blog->title }}</h3>
            
                <p class="text-justify">{{$blog->description }}</p>
                <span class="my-1 text-muted"><b>Author</b>: {{$blog->user->first_name}} {{$blog->user->last_name}}</span>
                <div class="mb-1 text-muted"><b>Date</b>:{{ $blog->created_at }}</div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>