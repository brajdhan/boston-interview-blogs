<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edit Blog</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

    @include('layouts.navbar')

    <div class="container my-5">
        <div class="row">
            <form action="{{ route('blogs.update', ['id' => $blog->id]) }}" method="POST" enctype="multipart/form-data">
                <div class="col-md-5 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="text-header text-center">Edit Blog</h2>
                            @if (session()->has('success'))
                            <div class="alert alert-success">{{ session()->get('success') }}</div>
                            @endif
                        </div>
                        <div class="card-body">
                            @csrf
                            @method('PUT')
                            <div class="mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input type="text" class="form-control" name="title" placeholder="Title" value="{{old('title', $blog->title)}}">
                                @error('title')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="description" class="form-label">Description</label>
                                <textarea class="form-control" name="description" placeholder="Description">{{old('description',$blog->description)}}</textarea>
                                @error('description')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <div>
                                    <img width="200" height="250" src="{{ asset('uploads/' . $blog->thumbnail) }}" alt="{{ $blog->title }}" class="img-fluid" />
                                </div>
                                <label for="thumbnail" class="form-label">Select Thumbnail</label>
                                <input type="file" class="form-control" name="thumbnail">
                                @error('thumbnail')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="mb-3 text-center">
                                <button type="submit" class="btn btn-primary w-50">Add Blog</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>