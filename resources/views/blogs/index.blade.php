<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Blogs</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
  
  @include('layouts.navbar')

  <div class="container my-3">
    <div class="row mb-2">
      <h2 class="text-center m-3">Blogs</h2>
      @forelse ($blogs as $blog)
      <div class="col-md-6">
        <div class="row g-0 border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
          <div class="col p-4 d-flex flex-column position-static">
            <h3 class="mb-0">{{ $blog->title }}</h3>
            <div class="mb-1 text-muted">{{ $blog->created_at }}</div>
            <p class="card-text mb-auto">{{ strlen($blog->description) > 300 ? substr($blog->description, 0, 300) . '...' : $blog->description }}</p>
            <p class="my-1 text-muted"><b>Author</b>: {{$blog->user->first_name}} {{$blog->user->last_name}}</p>
            <a href="{{route('blogs.show',['id'=>$blog->id])}}" class="my-2 stretched-link">Continue reading</a>
          </div>
          <div class="col-auto d-none d-lg-block">
            @if ($blog->thumbnail)
            <img width="200" height="250" src="{{ asset('uploads/' . $blog->thumbnail) }}" alt="{{ $blog->title }}" class="img-fluid" />
            @endif
          </div>
        </div>
      </div>
      @empty
      <p class="text-center">No blogs available.</p>
      @endforelse
      <!-- pagination -->
      {{ $blogs->links() }}
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
</body>
</html>