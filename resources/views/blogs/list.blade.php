<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Blogs List</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">
</head>
<body>

  @include('layouts.navbar')

  <div class="container my-3">
    <div class="row">
      <div class="col-sm-12">

        <div>
          <h2 class="text-center">
            Blogs List
            <a href="{{route('blog')}}" class="btn btn-secondary mx-2 float-end">Go to blogs</a>
            <a href="{{route('blogs.create')}}" class="btn btn-primary mx-2 float-end">Add Blog</a>
          </h2>
        </div>
        <br>

        @if (session()->has('success'))
        <div class="alert alert-success">{{ session()->get('success') }}</div>
        @endif

        @if (session()->has('error'))
        <div class="alert alert-danger">{{ session()->get('success') }}</div>
        @endif

        <div class="table-responsive">
          <table class="table" class="display" id="myTable" style="width:100%">
            <thead>
              <tr>
                <th>S.No</th>
                <th>Title</th>
                <th>Description</th>
                <th>date</th>
                <th>Author</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($blogs as $blog)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$blog->title}}</td>
                <td>{{$blog->description}}</td>
                <td>{{$blog->created_at}}</td>
                <td>{{$blog->user->first_name}} {{$blog->user->last_name}}</td>
                <td>
                  <a href="{{route('blogs.edit', $blog->id)}}" class="btn btn-primary  btn-sm my-2">Edit</a>
                  <a href="{{route('blogs.delete', $blog->id)}}" class="btn btn-danger btn-sm my-2">Delete</a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js"></script>
  <script>
    $(document).ready(function() {
      $('#myTable').DataTable();
    });
  </script>
</body>
</html>