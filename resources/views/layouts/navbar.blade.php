

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="{{route('blog')}}">BostonInterview</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link" href="{{route('blog')}}">Blogs</a>
          </li>
          @if (auth()->check())
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="{{route('blogs.list')}}">Blog List</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{route('auth.logout')}}">Logout</a>
          </li>
          @else
          <li class="nav-item">
            <a class="nav-link" href="{{route('auth.login')}}">Login</a>
          </li>
          @endif
        </ul>
      </div>
    </div>
  </nav>