<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('register', [\App\Http\Controllers\Auth\UserController::class, 'index'])->name('auth.register');
Route::post('register', [\App\Http\Controllers\Auth\UserController::class, 'store'])->name('auth.register.store');
Route::get('login', [\App\Http\Controllers\Auth\UserController::class, 'login'])->name('auth.login');
Route::post('login', [\App\Http\Controllers\Auth\UserController::class, 'authenticate'])->name('auth.login.authenticate');

//blog No Auth 
Route::get('/', [\App\Http\Controllers\BlogController::class, 'index'])->name('blog');
Route::get('blogs', [\App\Http\Controllers\BlogController::class, 'index'])->name('blog');
Route::get('blog/show/{id}', [\App\Http\Controllers\BlogController::class, 'show'])->name('blogs.show');

//blog Auth
Route::middleware(['auth'])->group(function () {
    Route::get('blog/create', [\App\Http\Controllers\BlogController::class, 'create'])->name('blogs.create');
    Route::post('blog/create', [\App\Http\Controllers\BlogController::class, 'store'])->name('blogs.store');
    Route::get('blog/list', [\App\Http\Controllers\BlogController::class, 'blogList'])->name('blogs.list');
    Route::get('blog/edit/{id}', [\App\Http\Controllers\BlogController::class, 'edit'])->name('blogs.edit');
    Route::put('blog/update/{id}', [\App\Http\Controllers\BlogController::class, 'update'])->name('blogs.update');
    Route::get('blog/delete/{id}', [\App\Http\Controllers\BlogController::class, 'destroy'])->name('blogs.delete');
    
    Route::get('logout', [\App\Http\Controllers\Auth\UserController::class, 'logout'])->name('auth.logout');
});