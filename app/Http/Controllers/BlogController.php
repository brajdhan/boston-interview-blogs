<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    //index method show blogs
    public function index()
    {   
        $blogs = Blog::paginate(10);
        return view('blogs.index', compact('blogs'));
    }

    //create method blog form
    public function create()
    {
        return view('blogs.create');
    }

    //store method store blog
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string|min:10',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
    
        if ($request->hasFile('thumbnail')) {
            $image = $request->file('thumbnail');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
        }

        $blog = new Blog();
        $blog->title = $request->title;
        $blog->user_id = auth()->user()->id;
        $blog->description = $request->description;
        $blog->thumbnail = $imageName;
        $blog->save();

        return redirect()->route('blogs.create')->with('success', 'Blog created successfully.');
    }

    //create method show blog list
    public function blogList()
    {
        $blogs = Blog::all();
        return view('blogs.list', compact('blogs'));
    }

    //create method show blog
    public function show($id)
    {
        $blog = Blog::find($id);
        return view('blogs.show', compact('blog'));
    }

    //create method show edit blog form
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('blogs.edit', compact('blog'));
    }

    //create method update blog
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string',
            'description' => 'required|string|min:10',
        ]);

        $blog = Blog::find($id);
        if ($request->hasFile('thumbnail')) {
            $request->validate([
                'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
            //upload image
            $image = $request->file('thumbnail');
            $imageName = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $imageName);
        }else{
            $imageName = $blog->thumbnail;
        }

        $blog->title = $request->title;
        $blog->description = $request->description;
        if ($request->hasFile('thumbnail')) {
            $blog->thumbnail = $imageName;
        }
        $blog->save();

        return redirect()->back()->with('success', 'Blog updated successfully.');
    }

    //create method delete blog
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();

        return redirect()->back()->with('success', 'Blog deleted successfully.');
    }

}
