<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    // Display registration form
    public function index()
    {
        return view('auth.register');
    }

    // user's registration
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'mobile' => 'required|numeric|unique:users,mobile',
            'password' => 'required|string|min:8',
        ]);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->mobile = $request->mobile;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect()->route('auth.login')->with('success', 'User created successfully.');
    }

    // Display login form
    public function login()
    {
        if (Auth::check()) {
            return redirect()->route('blog');
        }
        return view('auth.login');
    }

    // user's login
    public function authenticate(Request $request)
    {
        $request->validate([
            'mobile' => 'required|exists:users,mobile',
            'password' => 'required|string|min:8',
        ]);

        $user = User::where('mobile', $request->mobile)->first();
        if (Hash::check($request->password, $user->password)) {
            if (Auth::attempt(['mobile' => $request->mobile, 'password' => $request->password])) {
                $request->session()->put('user_id', Auth::user()->id);
                return redirect()->intended('/');
            }
        }
        return redirect()->route('auth.login')->with('error', 'Invalid credentials.');
    }

    // user's logout
    public function logout()
    {
        Auth::logout();
        Session::forget('user_id');
        return redirect('/login');
    }
}
